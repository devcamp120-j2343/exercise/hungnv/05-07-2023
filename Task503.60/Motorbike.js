import Vehicle from "./Vehicle.js";
class Motorbike extends Vehicle{
    constructor(vId, modelName) {
        super();
        this.vId = vId;
        this.modelName = modelName;
    }
    honk() {
        console.log("Motorbike is honking.");
    }
}
export default Motorbike;