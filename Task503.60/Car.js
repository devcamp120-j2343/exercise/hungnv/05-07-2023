import Vehicle from "./Vehicle.js";
class Car extends Vehicle {
    constructor(vId, modelName) {
        super();
        this.vId = vId;
        this.modelName = modelName;
    }
    honk() {
        console.log("Car is honking.");
    }
}
export default Car;