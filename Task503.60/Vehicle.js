class Vehicle {
    constructor(brand, yearManufactured) {
        this.brand = brand;
        this.yearManufactured = yearManufactured;
    }
    print() {
        console.log("brand" + this.brand);
        console.log("yearManufactured" + this.yearManufactured);
    }
}
export default Vehicle;